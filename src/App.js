import { Route } from 'react-router';
import './App.css';
// import Courses from './Courses';
import  Home  from './Home';
import {BrowserRouter as Router } from "react-router-dom";
import Success from './Success-stories';
import Contact from './Contact';
import Java from './Java';



function App() {
  return (
    <Router>
    <div className="App">
      
      <Route exact path="/" component={Home} />
      {/* <Route exact path="/Courses" component={Courses} /> */}
      <Route exact path="/success-stories" component={Success} />
      <Route exact path="/Contact" component={Contact} />
      <Route exact path="/Java" component={Java} />




     
    </div>
    </Router>
  );
}

export default App;
