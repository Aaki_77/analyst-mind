

import {React} from 'react';
import {BrowserRouter as Router,Link } from "react-router-dom";
import './Home.css';
import {Row, Col } from 'react-grid-system';
import AnalystMind from './assests/AnalystMind.jpg';
import Aakash from './assests/Aakash.jpg';
import Rohit from './assests/Rohit.jpg';
import Card from "@material-ui/core/Card";



 const Home = () => {
    return (
        <Router>

<div className="container">
        <div >
           
            <Row className="row"> 
            <Col className="row">
            <Link to="/" className="row" target="_blank">Home</Link>
            </Col>

            <Col>
            <Link to="/Courses" className="row" target="_blank">Courses</Link>
            </Col>

            <Col>
            <Link to="/Success-stories" className="row" target="_blank">Success Stories</Link>
            </Col>

            <Col>
            <Link to="/Contact" className="row" target="_blank">Contact</Link>
            </Col>
        </Row>
        </div>
        <br />


        </div>
            <div className="header">
            
                <header>
                    Welcome to Analyst-Mind
                </header>

              
            <img src ={AnalystMind} width="100%" alt=""/>
      
                
            </div>
        

            <div className="strong">
                    <strong > Build your Skill-Sets WithIn 90 Days!! </strong><br/>
                    <strong> 100% Job Assurance !! </strong>
            </div>

{/* success Stories */}
<div>
    <Row>
        <Col>
            <Card>
               
                <img src={Aakash} width="50%" alt=""/>
                </Card>
                </Col>
                <Col>
                <Card>
                
                <img src={Rohit} width="72.2%" alt=""/>
               
                
                </Card>
                </Col>
                </Row>
                </div>
                <br/>


{/* courses */}

                <div className="courses">
                 <strong> COURSES </strong>
                 <Row>
        <Col>
            <Card>  
                <strong className="strong1">Analyst-Mind Platinum Module</strong>
                <br/><br />
                <a href="/Java">
                <button className="button">JAVA & SPRINGBOOT</button>
                </a>
                </Card>
                </Col>

               
                <Col>
                <Card >
                <strong className="strong1">Analyst-Mind DataScience Module</strong>
                <br/>
                <a href="/DataScience">
                <button className="button" >DATASCIENCE</button>
                </a>
                
                </Card>
                </Col>
                </Row>

                    </div>


            

        
       <div className="paragraph">
        <p >
            <h1> © 2021 Analyst Mind Pvt. Ltd </h1>
           
            <h2> With  the huge demand for skilled resources, we believe our products are helping the users to upskill and we are adding more to the content every day possible to keep the user in sync with the current IT needs and not just this we have our in-house classroom training which specially caters to those interested candidates/Freshers who want to learn based on a practical approach and become better at the skill they want to make a career in, Along with our in-house classroom training programs we also give numerous placement opportunities for the students enrolled which will allow them to get their first dream job.

       </h2>
       
        </p>
        </div>

        <footer className="footer">
            All Rights reserved  ®.
        </footer>
      
      
       </Router>
    );
};

export default Home;
