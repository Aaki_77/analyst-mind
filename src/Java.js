import {BrowserRouter as Router,Link } from "react-router-dom";
import './Home.css';
import {Row, Col } from 'react-grid-system';
import { Card } from "@material-ui/core";



const Courses = () => {

    return (
        <Router>

        <div className="container">
                <div >
                   
                    <Row className="row"> 
                    <Col className="row">
                    <Link to="/" className="row" target="_blank">Home</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Courses" className="row" target="_blank">Courses</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Success-stories" className="row" target="_blank">Success Stories</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Contact" className="row" target="_blank">Contact</Link>
                    </Col>
                </Row>
                </div>
                <br />
        
</div>

{/* <div>
<Card>
<strong> DataScience Module</strong>
</Card>
</div> */}

<div>
<Card>
<strong>JAVA & SPRINGBOOT Module</strong>

<Card>
<b>CORE JAVA</b><br/>
<p>

<strong>Java </strong><br/>
<h7>Along with brief history, get acquainted with terminology of the language, get to know the features which enable Java to stand out</h7><br/><br/>

<strong>Object Orientation</strong><br/>
<h7>Explore the inside of an object, however, make sure to protect it from the garbage collector thread</h7> <br/><br/>

<strong>Data Types</strong><br/>
<h7>Make storage of data more efficient, learn the wiz art of typecasting</h7> <br/><br/>

<strong>Working of Loops</strong><br/>
Believer in smart work instead of hard work? Loops are a MUST in your logic armory <br/><br/>

<strong>Arrays in Java</strong><br/>
Use a SINGLE variable name to access tonnes of data. But wait, there are some limitations! <br/><br/>

<strong>String in Java</strong><br/>
Who doesn’t wish to play strings? But not everyone knows how to! Watch String, StringBuffer and StringBuilder fight for significance <br/><br/>

<strong>Method in Java</strong><br/>
Methods live for the team. Fundamentals of almost every modern programming language, methods are born on the stack, execute and diminish <br/><br/>

<strong>Method overloading</strong><br/>
Multiple methods with the same name- sounds polymorphism but is it? <br/><br/>

<strong>Class Members in Java</strong><br/>
You really thought main() was the entry point of a program? Well, say hello to static. learn to use static variables, static blocks as well as static methods <br/><br/>

<strong>Encapsulation</strong><br/>
Protect your private data using public getters and setters. Don’t like the name setters? How about Constructors? Interested in constructor chaining?  <br/><br/>

<strong>Super</strong><br/>
Know about super keyword  <br/><br/>

<strong>This</strong><br/>
Know about this keyword <br/><br/>

<strong>Constructor overloading</strong><br/>
Learn how to instantiate different objects with different states <br/><br/>

<strong>Static keyword</strong><br/>

<strong>Inheritance</strong><br/>
Acquire the properties and behavior of the parents, but, for what? Save time using inheritance-, codeless and earn more <br/><br/>

<strong>Single inheritance</strong><br/>
Enables a derived class to inherit properties and behavior from a single parent class  <br/><br/>

<strong>Multiple inheritance</strong><br/>
Inherit properties of more than one parent class  <br/><br/>

<strong>Multilevel inheritance</strong><br/>
Derived class will be inheriting a base class and as well as the derived class also act as the base class to other class <br/><br/>

<strong>Cyclic inheritance</strong><br/>
Class is its superclass and subclass at the same time <br/><br/>

<strong>Hybrid inheritance</strong><br/>
Achieve hybrid inheritance only through Interfaces. <br/><br/>

<strong>Method overriding</strong><br/>
If a subclass has the same method as declared in the parent class, it is known as method overriding in Java <br/><br/>

<strong>Rules of method overriding</strong><br/>
Learn how to override a method <br/><br/>

<strong>Final keyword</strong><br/>
Want to stop inheritance, wanna create constant, how about stopping overriding? Make a method final in the abstract class and check!  <br/><br/>

<strong>Polymorphism</strong><br/>
1 to many! Use polymorphism concepts to not only reduce the length of your code and make it compact but also make it super flexible  <br/><br/>

<strong>Abstraction and Interfaces</strong><br/>
There must be something special about these interfaces that along with class names and constructors, they are also allowed Upper case initials. Are they of equal significance as a class?  <br/><br/>

<strong>Packages</strong><br/>
Who all should know the location of the locker? Who should have the keys? Specify access by practicing the concept of packages. Do we have a standard format for creating a package? How many inbuilt packages do you know and why is the package kept so? <br/><br/>

<strong>ADVANCE JAVA</strong><br/>

<strong>Introduction to  Exception Handling</strong><br/>
We are mere programmers and bugs are bound to occur even after utmost sincerity. Handle exceptions as prevention is better than cure  <br/><br/>

<strong>Handling the exception</strong><br/>
Intrusted to learn try-catch <br/><br/>

<strong>Rethrowing the exception</strong><br/>
Can we throw the exception object? <br/><br/>

<strong>Ducking the exception</strong><br/>
What happens when an exception is not handled properly <br/><br/>

<strong>Custom exceptions</strong><br/>
Can a programmer create exception according to project demand? <br/><br/>

<strong>Exception hierarchy</strong><br/>
Does exception have a hierarchy?  <br/><br/>

<strong>Liskov’s substitution principle</strong><br/>
Try overriding methods when exceptions are involved  <br/><br/>

<strong>Thread and Multi-threading</strong><br/>
Speed is essential in today’s world. Give your application an extra wheel by using the concepts of multi-threading  <br/><br/>

<strong>Race condition</strong><br/>
Learn the challenges of a multithreaded environment  <br/><br/>

<strong>Deadlock</strong><br/>
Can multiple threads access the same resource at a time? if yes do we have any challenges associated with it ?  <br/><br/>

<strong>Producer consumer problem</strong><br/>
Is multithreading applicable in all cases ? is race condition good or bad? Can we make threads to communicate with each other ? <br/><br/>

<strong>Thread life cycle</strong><br/>
Even threads have a life cycle! interesting …well learn about states of a thread  <br/><br/>

<strong>Networking</strong><br/>
Learn how to make communication happen between two processes via socket programming  <br/><br/>
 
<strong>Serialization</strong><br/>
Interested in making the object’s state persistent? well, learn how?  <br/><br/>

<strong>Generics & Collections</strong><br/>
Warned you about the limitations of arrays, didn’t we? Well, why fear when collections are here. Make storage, retrieval as well as searching efficient by using the gifts of the Collections framework  <br/><br/>

<strong>Concurrent collections</strong><br/>
Want to write better concurrent java applications? then use concurrent collections <br/><br/>

<strong>Jdk 9 new features</strong><br/>
Private interface methods, Stream API improvements, Collection factory methods  <br/><br/>
 
<strong>Jdk 10 new features</strong><br/>
Local-Variable Type Inference, Thread-Local Handshakes <br/><br/>

<strong>Jdk 11 new features</strong><br/>
Type Inference for Lambda Parameters, String:: lines, New File Methods <br/><br/>

<strong>Jdk 12 new features-Switch Expressions, File mismatch() Method, Strings New Methods, Pattern Matching for instanceof </strong><br/><br/>

<strong>J2EE INTRODUCTION</strong><br/>
More than 90% of applications on the internet are web applications and that is where the Java Enterprise Edition comes into the picture <br/><br/>

<strong>JDBC</strong><br/>
Make the communication between your java code and database possible by using the concepts of JDBC <br/><br/>

<strong>Servlets</strong><br/>
Looks and feels of a web app is all-important but this is how and where you write the business logic. The BACK END- Servlets <br/><br/>

<strong>JSP</strong><br/>
Learn this amazing server page technology JSP and ensure the user wants to visit your web app again and again <br/><br/>

<strong>JAVA FRAMEWORKS</strong><br/>

<strong>Spring</strong><br/>
Learn this to Solve difficulties in Enterprise application development <br/><br/>

<strong>Hibernate</strong><br/>
Facing difficulty in learning SQL or fed up with writing repetitive JDBC code? then learn hibernate <br/><br/>
</p>

    </Card>
</Card>
</div>


</Router>   
    );

};

export default Courses;