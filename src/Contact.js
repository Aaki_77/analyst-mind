import {BrowserRouter as Router,Link } from "react-router-dom";
import './Home.css';
import {Row, Col } from 'react-grid-system';



const Contact = () => {

    return (
        <Router>

        <div className="container">
                <div >
                   
                    <Row className="row"> 
                    <Col className="row">
                    <Link to="/" className="row" target="_blank">Home</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Courses" className="row" target="_blank">Courses</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Success-stories" className="row" target="_blank">Success Stories</Link>
                    </Col>
        
                    <Col>
                    <Link to="/Contact" className="row" target="_blank">Contact</Link>
                    </Col>
                </Row>
                </div>
                <br />
        
</div>
</Router>   
    );

};

export default Contact;